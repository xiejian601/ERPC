cmake_minimum_required(VERSION 3.8)

add_executable(app5 app.c security.c)
add_executable(service5 service.c security.c)
target_link_libraries(app5 ${Libraries})
target_link_libraries(service5 ${Libraries})
